// creamos la función controller que va a recibir la vista y los datos (libros)
var controller = function controller(view, products) {

    // definimos el evento onFilter en el objeto vista
    view.onFilter = function onFilter( /*filterName,*/ filterColor, filterModel, filterSize, filterDesigner, filterType) {
        // creamos la variable librosFiltrados
        var casesFiltrados = products

            // filtro por color
            .filter(function (elemento) {
                // si filterYear está vacío retornamos true
                if (!filterColor) return true;
                // si no, retornamos el resultado de la comparación
                return elemento.color == filterColor;
            })
            // filtro por páginas
            .filter(function (elemento) {
                // si filterPages está vacío retornamos true
                if (!filterModel) return true;
                return elemento.model == filterModel;

                // como el valor de filterPages es un rango separado por un guión,
                // utilizamos la función split para acceder a los 2 valores
                //var rango = filterPages.split('-');

                // retornamos el resultado de la comparación con el rango
                //return elemento.pages >= rango[0] && elemento.pages < rango[1];
            })
            // filtro por tamaño
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterSize) return true;

                // retornamos el resultado de la comparación
                return elemento.size == filterSize;
            })
            // filtro por público
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterDesigner) return true;

                // retornamos el resultado de la comparación
                return elemento.designer == filterDesigner;
            })
            // filtro por tipo
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterType) return true;

                // retornamos el resultado de la comparación
                return elemento.type == filterType;
            })
        // filtro por name
        /*.filter(function (elemento){
          // si filterPublic está vacío retornamos true
          if(!filterName) return true;


            var palabra = elemento.nombre;
            var texto = filterName;
            var resultado = texto.contains(palabra);

            if(resultado){
                console.log(texto);
            }else{
                console.log(texto);
            }
                      // retornamos el resultado de la comparación
          //return elemento.nombre == filterName;
            return resultado;
        })*/
        ;

        // renderizamos con la variable librosFiltrados
        view.render(casesFiltrados);
    }

    // render inicial con todos los libros
    //view.render(products);
    // llamamos a setHeaderEvents
    view.setHeaderEvents();
}

// llamamos la función controller y le pasamos la vista y los datos
controller(view, products);


// creamos la función controller que va a recibir la vista y los datos (libros)
var ctr = function ctr(cartItems, cart) {

    // definimos el evento onFilter en el objeto vista

    cartItems.onFilter = function onFilter( filterName, filterColor, filterModel, filterSize, filterDesigner, filterType) {
        // creamos la variable librosFiltrados
        var casesFiltrados = cart

            // filtro por color
            .filter(function (elemento) {
                // si filterYear está vacío retornamos true
                if (!filterColor) return true;
                // si no, retornamos el resultado de la comparación
                return elemento.color == filterColor;
            })
            // filtro por páginas
            .filter(function (elemento) {
                // si filterPages está vacío retornamos true
                if (!filterModel) return true;
                return elemento.model == filterModel;

                // como el valor de filterPages es un rango separado por un guión,
                // utilizamos la función split para acceder a los 2 valores
                //var rango = filterPages.split('-');

                // retornamos el resultado de la comparación con el rango
                //return elemento.pages >= rango[0] && elemento.pages < rango[1];
            })
            // filtro por tamaño
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterSize) return true;

                // retornamos el resultado de la comparación
                return elemento.size == filterSize;
            })
            // filtro por público
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterDesigner) return true;

                // retornamos el resultado de la comparación
                return elemento.designer == filterDesigner;
            })
            // filtro por tipo
            .filter(function (elemento) {
                // si filterPublic está vacío retornamos true
                if (!filterType) return true;

                // retornamos el resultado de la comparación
                return elemento.type == filterType;
            })
        // filtro por name
        .filter(function (elemento){
          // si filterPublic está vacío retornamos true
          if(!filterName) return true;


            var palabra = elemento.nombre;
            var texto = filterName;
            var resultado = texto.contains(palabra);

            if(resultado){
                console.log(texto);
            }else{
                console.log(texto);
            }
                      // retornamos el resultado de la comparación
          //return elemento.nombre == filterName;
            return resultado;
        })
        ;

        // renderizamos con la variable librosFiltrados
        cartItems.render(casesFiltrados);
    }

    // render inicial con todos los libros
    cartItems.render(cart);
    // llamamos a setHeaderEvents
    //cartItems.setHeaderEvents();
}

// llamamos la función controller y le pasamos la vista y los datos
ctr(cartItems, cart);

