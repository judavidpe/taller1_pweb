var cartItems = {
    // la misma función getLibro pero sin los comentarios
    getElemCas: function getElemCas(infoCas) {
        var div = document.createElement('div');
        div.setAttribute('class', 'cas');
        div.innerHTML = `<div class='cn'>
			<img src='${infoCas.urlImage}' class='img-responsive' />
			<h3>${infoCas.nombre}</h3>
			<p>\$ ${infoCas.price}</p>
            <!--<button>More</button>---></div>
		`;

        console.log(div);
        return div;
    },

    getElemCas: function getElemCas(listaCas) {
        var div = document.createElement('div');
        div.setAttribute('class', 'row');

        var that = this;
        listaCas.forEach(function (infoCas) {
            var li = that.getElemCas(infoCas);
            div.appendChild(li);
        });

        return div;
    },

    render: function render(listaCas) {
        var mai = document.getElementById('cont-cart');

        mai.setAttribute('class', 'cont-carts');

        var elemCas = this.getElemCas(listaCas);
        // comentamos esta línea
        // var elemHeader = this.getElemHeader();

        mai.innerHTML = '';
        // y esta línea
        // main.appendChild(elemHeader);
        mai.appendChild(elemCas);
    }
};
