var view = {
    // la misma función getLibro pero sin los comentarios
    getElemCase: function getElemCase(infoCase) {
        var div = document.createElement('div');
        div.setAttribute('class', 'case');
        var opacity;
        if(infoCase.inventory <= 0){
           opacity = 1;
           }else {
               opacity = 0;
           };
        div.innerHTML = `<div class="cn">
<span class="vert line"></span><span class="hori line"></span>
			<img src="${infoCase.urlImage}" class="img-responsive" />
            <span class="ava" style="opacity: ${opacity}">SOLD OUT</span>
			<h3>${infoCase.nombre}</h3>
			<p>\$ ${infoCase.price}</p>
            <!--<button>More</button>---></div>
		`;

        var that = this;

        div.querySelector('.cn').addEventListener('click', function () {
            var modal = that.getModalCase(infoCase);
            div.appendChild(modal);
        });
        return div;
    },

    getModalCase: function getModalCase(infoCase) {
        var div = document.createElement('div');
        var designer = infoCase.designer;
        var size = infoCase.size;
        var color = infoCase.color;

        var toPut;
        var toPutSize;
        var toPutColor;

        if (designer == 'lindsay') {
            toPut = 'Lindsay Gardner';
        } else if (designer == 'lubbo') {
            toPut = 'Andrés Lubbo';
        } else if (designer == 'arturo') {
            toPut = 'Arturo Gómez';
        };


        if (size == '11inch') {
            toPutSize = '11 inch';
        } else if (size == '13inch') {
            toPutSize = '13 inch';
        } else if (size == '15inch') {
            toPutSize = '15 inch';
        } else if (size == '12inch') {
            toPutSize = '12 inch';
        };

        if (color == 'blue') {
            toPutColor = '#336F93';
        } else if (color == 'red') {
            toPutColor = '#9B1F33';
        } else if (color == 'yellow') {
            toPutColor = '#E2A93F';
        } else if (color == 'green') {
            toPutColor = '#31AF70';
        } else if (color == 'black') {
            toPutColor = '#111111';
        } else if (color == 'white') {
            toPutColor = '#dddddd';
        };


        div.setAttribute('class', 'window-back');
        div.innerHTML = `


        <div class="window-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">${infoCase.nombre}</h4>
                <img src="${infoCase.urlImage}" alt="Image ${infoCase.nombre}"/>
                <p class="price-window">\$ ${infoCase.price}</p>
                <p class="des">${infoCase.description}</p>
                <span id="feedC" style="opacity:0">Added</span>
                <button class="addToCart">Add to cart</button>
                <p class="datosAdd">${toPut} · ${toPutSize} <span class="circle" style="background: ${toPutColor} "></span></p>
        </div><!-- /.modal-content -->
    `;

        // seleccionamos el elemento con la clase modal
        var modal = div.querySelector('.window-content');
        // seleccionamos el elemento con la clase modal-backdrop
        // var backdrop = div.querySelector('.modal-backdrop');

        div.style.pointerEvents = 'none';
        div.style.zIndex = '99999';
        div.style.trasition = 'all ease 0.3s';

        // display = 'block' debe suceder antes
        modal.style.display = 'block';
        modal.style.background = '#ffffff';
        modal.style.boxSizing = 'border-box';
        modal.style.boxShadow = '0px 0px 50px rgba(0,0,0,0.5)';
        modal.style.margin = '0 auto';
        modal.style.maxWidth = '800px';
        modal.style.maxHeight = '600px';
        modal.style.padding = '50px';
        modal.style.opacity = '1';
        modal.style.trasition = 'all ease 0.3s';
        modal.style.width = '90%';

        var that = this;

        div.querySelector('.addToCart').addEventListener('click', function () {
            //var modal = that.getModalCase(infoCase);
            //div.appendChild(modal);
            console.log(infoCase.nombre + " " + infoCase.price);

            var feedb = document.getElementById('feedC');
            feedb.style.opacity = '1';

            var x = cart;
            x.push(infoCase);
            //console.log(infoCase.inventory)
            infoCase.inventory -= 1;
           // console.log(infoCase.inventory)

            console.log(x);
            console.log(cart.length);
            if (cart.length > 0) {
                console.log(cart.length);
                var div = document.querySelector('#carro');
                div.innerHTML = `Cart <span class="icon-car"></span><span class="contCart" id="contCart">${cart.length}</span>
    `;
            } else{

            }
        });
        // return div;

        setTimeout(function () {
            // pasamos ambos opacity al setTimeout
            modal.style.opacity = 1;

            div.style.pointerEvents = 'all';
            div.style.trasition = 'all ease 0.3s';
            // backdrop.style.opacity = .7;
            // div.querySelector('.modal-dialog').style.transform = 'translate(0,0)';
        });

        var remove = function () {
            // pasamos opacity a 0 inmediatamente para iniciar la animación
            modal.style.opacity = 0;
            modal.style.trasition = 'all ease 0.3s';
            //backdrop.style.opacity = 0;
            setTimeout(function () {
                // eliminamos el elemento después de 300 milisegundos
                div.remove();
            }, 300);
        };
        div.addEventListener('click', function (e) {
            if (e.target == modal) remove();
        });
        modal.addEventListener('click', function (e) {
            if (e.target == modal) remove();
        });

        div.querySelector('button').addEventListener('click', remove);

        return div;
    },


    getElemCases: function getElemCases(listaCases) {
        var div = document.createElement('div');
        div.setAttribute('class', 'row');

        var that = this;
        listaCases.forEach(function (infoCase) {
            var li = that.getElemCase(infoCase);
            div.appendChild(li);
        });

        return div;
    },

    setHeaderEvents: function setHeaderEvents() {
        var form = document.querySelector('#main');

        var that = this;

        // var filterByName = form.querySelector('#nombreProducto');
        var filterByColor = form.querySelector('#selectColor');
        var filterByModel = form.querySelector('#selectModel'); // nuevo
        var filterBySize = form.querySelector('#selectSize'); // nuevo
        var filterByDesigner = form.querySelector('#selectDesigner'); // nuevo
        var filterByType = form.querySelector('#selectType'); // nuevo

        // guardamos la función en la variable handler
        var handler = function () {
            // pasamos todos los valores
            that.onFilter( /*filterByName.value,*/ filterByColor.value, filterByModel.value, filterBySize.value, filterByDesigner.value, filterByType.value);
        }

        // pasamos la variable handler al addEventListener de los 3 filtros
        //filterByName.addEventListener('change', handler);
        filterByColor.addEventListener('change', handler);
        filterByModel.addEventListener('change', handler);
        filterBySize.addEventListener('change', handler);
        filterByDesigner.addEventListener('change', handler);
        filterByType.addEventListener('change', handler);
    },

    render: function render(listaCases) {
        var main = document.getElementById('content');

        main.setAttribute('class', 'container');

        var elemCases = this.getElemCases(listaCases);
        // comentamos esta línea
        // var elemHeader = this.getElemHeader();

        main.innerHTML = '';
        // y esta línea
        // main.appendChild(elemHeader);
        main.appendChild(elemCases);
    }
};
